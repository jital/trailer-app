package hello.datasource;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.Lists;
import hello.datasource.datamodel.VideoData;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class ImdbDataSource implements Datasource {
    private static final String url = "http://www.omdbapi.com";
    private static final String apikey = "1b0fbdd";
    private static final ObjectMapper mapper = new ObjectMapper();
    LoadingCache<String, String> imdbCache = CacheBuilder.newBuilder()
            .maximumSize(1000)
            .expireAfterWrite(100, TimeUnit.MINUTES)
        .build(new CacheLoader<String, String>() {
            @Override
            public String load(String s) throws Exception {
                return callApi(s, 1);
            }
        });
    private YoutubeDataSource yds;

    public ImdbDataSource(YoutubeDataSource yds) {
        this.yds = yds;
    }

    @Override
    public List<VideoData> getData(String s) {
        List<VideoData> vids = Lists.newArrayList();

        try {
            String response = imdbCache.get(s);
            vids.addAll(parseResponse(response));
        } catch (IOException | ExecutionException e) {
            e.printStackTrace();
        }
        return vids;
    }

    private List<VideoData> parseResponse(String response) throws JsonProcessingException {
        List<VideoData> vids = Lists.newArrayList();
        JsonNode root = mapper.readTree(response);
        JsonNode items = root.path("Search");
        for (JsonNode movieRes : items) {
            vids.add(new VideoData(movieRes.path("Title").textValue(),
                    movieRes.path("imdbID").textValue(), movieRes.path("Poster").textValue()));
        }
        return vids;
    }

    public List<VideoData> fetchAll(String searchString) throws IOException {
        List<VideoData> allVids = Lists.newArrayList();;
        int lastVidCount = 0;
        int page = 1;
        do {
            String response = callApi(searchString, page);
            List<VideoData> vidsLocal = parseResponse(response);
            lastVidCount = vidsLocal.size();
            allVids.addAll(vidsLocal);
            page += 1;
        } while (lastVidCount > 0);
        return allVids;
    }

    private String callApi(String s, int page) throws IOException {
        System.out.println("API call executing");
        String charset = "UTF-8";  // Or in Java 7 and later, use the constant: java.nio.charset.StandardCharsets.UTF_8.name()
        String query = String.format("type=movie&apikey=%s&s=%s&page=%s",
                URLEncoder.encode(apikey, charset),
                URLEncoder.encode(s, charset),
                URLEncoder.encode(Integer.toString(page), charset));
        URLConnection connection = new URL(url + "?" + query).openConnection();
        connection.setRequestProperty("Accept-Charset", charset);
        InputStream response = connection.getInputStream();
        Scanner scanner = new Scanner(response).useDelimiter("\\A");
        return scanner.hasNext() ? scanner.next() : "";
    }

    public List<VideoData> getDataWithTrailer(String s) {
        List<VideoData> videos = Lists.newArrayList();
        try {
            String response = imdbCache.get(s);
            videos.addAll(parseResponse(response));
            for (VideoData vid: videos) {
                vid.setVideoLink(yds.fetchTrailerData(vid.getTitle()).getVideoLink());
            }
        } catch (IOException | ExecutionException e) {
            e.printStackTrace();
        }
        return videos;
    }

}
