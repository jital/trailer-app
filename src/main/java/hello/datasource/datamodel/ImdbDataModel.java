package hello.datasource.datamodel;

public class ImdbDataModel extends VideoData{

    public ImdbDataModel(String title, String imdbId, String videoLink) {
        super(title, imdbId, videoLink);
    }
}
