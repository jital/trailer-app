package hello.datasource.datamodel;

import java.util.Objects;

public class VideoData {
    private String title;
    private String imdbId;
    private String videoLink;

    public VideoData(String title, String imdbId, String videoLink) {
        this.title = title;
        this.imdbId = imdbId;
        this.videoLink = videoLink;
    }

    public VideoData() {

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImdbId() {
        return imdbId;
    }

    public void setImdbId(String imdbId) {
        this.imdbId = imdbId;
    }

    public String getVideoLink() {
        return videoLink;
    }

    public void setVideoLink(String videoLink) {
        this.videoLink = videoLink;
    }

    @Override
    public String toString() {
        return "VideoData{" +
                "title='" + title + '\'' +
                ", imdbId='" + imdbId + '\'' +
                ", videoLink='" + videoLink + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VideoData videoData = (VideoData) o;
        return Objects.equals(getTitle(), videoData.getTitle()) &&
                Objects.equals(getImdbId(), videoData.getImdbId()) &&
                Objects.equals(getVideoLink(), videoData.getVideoLink());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTitle(), getImdbId(), getVideoLink());
    }
}
