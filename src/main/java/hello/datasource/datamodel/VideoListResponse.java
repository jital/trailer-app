package hello.datasource.datamodel;

import java.util.List;

public class VideoListResponse {
    private List<VideoData> vidList;
    private int length;

    public VideoListResponse(List<VideoData> vidList) {
        this.vidList = vidList;
        this.length = vidList.size();
    }

    public List<VideoData> getVidList() {
        return vidList;
    }

    public void setVidList(List<VideoData> vidList) {
        this.vidList = vidList;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }
}
