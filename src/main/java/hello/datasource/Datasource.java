package hello.datasource;

import hello.datasource.datamodel.VideoData;

import java.util.List;

public interface Datasource {
    List<VideoData> getData(String s);
}
