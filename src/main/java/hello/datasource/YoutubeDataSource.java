package hello.datasource;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import hello.datasource.datamodel.VideoData;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.List;
import java.util.Scanner;

public class YoutubeDataSource implements Datasource{
    String apiKey = "AIzaSyBIgedGReYICVxQZQaku6-I7nkMz51-AKk";
    String url = "https://www.googleapis.com/youtube/v3/search";
    private static final ObjectMapper mapper = new ObjectMapper();

    public YoutubeDataSource() {
    }

    @Override
    public List<VideoData> getData(String s) {
        List<VideoData> vids = Lists.newArrayList();

        try {
            String response = callApi(s,1);
            vids = parseResponse(response);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return vids;
    }

    private List<VideoData> parseResponse(String response) throws JsonProcessingException {
        List<VideoData> vids = Lists.newArrayList();
        JsonNode root = mapper.readTree(response);
        JsonNode items = root.path("items");
        if (items.isArray()) {
            for (JsonNode item: items) {
//                    System.out.println(String.join(",", item.toPrettyString()));
                String videoId = item.get("id").get("videoId").asText();
                vids.add(new VideoData(item.get("snippet").get("title").asText(),
                        videoId, getVideoLinkFromId(videoId)));

            }
        }
        return vids;
    }

    private static String getVideoLinkFromId(String videoId) {
        return "https://www.youtube.com/watch?v=" + videoId;
    }

    private String callApi(String s, int page) throws IOException {
        String charset = "UTF-8";  // Or in Java 7 and later, use the constant: java.nio.charset.StandardCharsets.UTF_8.name()
        String query = String.format("type=video&part=%s&key=%s&q=%s",
                URLEncoder.encode("id,snippet", charset),
                URLEncoder.encode(apiKey, charset),
                URLEncoder.encode(s, charset));
        URLConnection connection = new URL(url + "?" + query).openConnection();
        connection.setRequestProperty("Accept-Charset", charset);
        InputStream response = connection.getInputStream();
        Scanner scanner = new Scanner(response).useDelimiter("\\A");
        return scanner.hasNext() ? scanner.next() : "";
    }

    VideoData fetchTrailerData(String movie) {
        List<VideoData> data = getData(movie + "Trailer");
        return selectMostSuitableTrailer(data, movie);
    }

    private VideoData selectMostSuitableTrailer (List<VideoData> data, String title){
        // TODO: Select on basis of most suitable title
        if (data.size() <= 0) return new VideoData();
        return data.get(0);
    }

}
