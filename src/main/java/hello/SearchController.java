package hello;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import hello.datasource.Datasource;
import hello.datasource.ImdbDataSource;
import hello.datasource.datamodel.VideoData;
import hello.datasource.YoutubeDataSource;
import hello.datasource.datamodel.VideoListResponse;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@RestController
public class SearchController {
    private static final ObjectMapper mapper = new ObjectMapper();
    private ImdbDataSource imdbDataSource;
    private YoutubeDataSource yds;

    public SearchController() {
        yds = new YoutubeDataSource();
        imdbDataSource = new ImdbDataSource(yds);
        yds = new YoutubeDataSource();
    }

    @RequestMapping("/search")
    public VideoListResponse search(@RequestParam String search_string) {

        List<VideoData> imdbResp = imdbDataSource.getDataWithTrailer(search_string);

        return new VideoListResponse(imdbResp);
    }
}