package hello.datasource;

import hello.datasource.datamodel.VideoData;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;


public class ImdbDataSourceTest {
    ImdbDataSource imdbDataSource;
    @Before
    public void setUp() throws Exception {
        imdbDataSource = new ImdbDataSource(new YoutubeDataSource());
    }

    @Test
    public void testCache() {
        String searchString = "June";
        List<VideoData> june = imdbDataSource.getData(searchString);
        List<VideoData> cached = imdbDataSource.getData(searchString);
        Assert.assertEquals(june, cached);
    }

    @Test
    public void testFetchAll() {
        String searchString = "June";
        List<VideoData> june = null;
        try {
            june = imdbDataSource.fetchAll(searchString);
            Assert.assertEquals(june.size(), 193);
        } catch (IOException e) {
            e.printStackTrace();
            Assert.fail();
        }
    }
    @Test
    public void testFetchTrailerData() {
        String searchString = "June";
        List<VideoData> june = null;
        june = imdbDataSource.getDataWithTrailer(searchString);
        System.out.println(june);
    }


}